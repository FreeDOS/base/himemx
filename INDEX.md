# HIMEMX

HimemX is a XMS memory manager derived from FreeDOS Himem.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## HIMEMX.LSM

<table>
<tr><td>title</td><td>HIMEMX</td></tr>
<tr><td>version</td><td>3.38</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-11-21</td></tr>
<tr><td>description</td><td>HimemX is a XMS memory manager derived from FreeDOS Himem.</td></tr>
<tr><td>summary</td><td>HimemX is a XMS memory manager derived from FreeDOS Himem.</td></tr>
<tr><td>keywords</td><td>XMS, himem, memory manager</td></tr>
<tr><td>author</td><td>Andreas "Japheth" Grech, Tom Ehlert, Till Gerken</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/HimemX</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://github.com/FDOS/himemX</td></tr>
<tr><td>original&nbsp;site</td><td>http://himemx.sourceforge.net/</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[General Public License, Artistic license and Public Domain](LICENSE)</td></tr>
</table>
